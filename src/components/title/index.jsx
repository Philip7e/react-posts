import "./title.css"

const Title = ({ name, text_align, type, classElement = "" }) => {
    return (
        <>
            {type === "h1" ?
                <h1 className={`${classElement === "" ? classElement : classElement + " "}title${text_align == "center" ? " text-center" : text_align == "right" ? " text-right" : " text-left"}`}> {name}</h1>
            : type === "h2" ?
                <h2 className={`${classElement === "" ? classElement : classElement + " "}title${text_align == "center" ? " text-center" : text_align == "right" ? " text-right" : " text-left"}`}> {name}</h2>
            : 
                <h3 className={`${classElement === "" ? classElement : classElement + " "}title${text_align == "center" ? " text-center" : text_align == "right" ? " text-right" : " text-left"}`}> {name}</h3>
            }
        </>

    )
}

export default Title;