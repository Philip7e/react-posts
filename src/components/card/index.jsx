import Title from "../title"
import "./card.css"

export const Card = ({description, title, id}) => {
    return (
        <div className="card">
            <Title name={title} type="h3" classElement="m-1"></Title>
            <div className="card-description">{description}</div>
            <div className="card-param">
                <div className="card-number">{id}</div>
                <div className="card-btn">
                    <button type="button">Console</button>
                </div>
            </div>
        </div>
    )
}