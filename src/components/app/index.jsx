import Title from "../title";
import { Cards } from "../cards";

import data from "../../data/posts.json";


const App = () => {
    return (
      <>
         <Title name="Список постів" text_align="center" type="h1"></Title>
         <Cards cards_element={data}></Cards>
      </>
    )
  }

export default App
  