import { Card } from "../card"

export const Cards = ({ cards_element }) => {
    return (
        <>
            {Array.isArray(cards_element) ?
                <div className="cards">
                    {
                        cards_element.map(({ body, title, id }) => {
                            return <Card key={id*(Math.random()*70)+"Cards"} description={body} title={title} id={id}></Card>
                        })
                    }
                </div> : <div className="cards-error">
                    Звантаження...
                </div>}
        </>

    )
}





